public class ItemVO {
	private int stock;
	private String name;
	private String id;

	public ItemVO(int stockk, String name, String id) {
		setStock(stockk);
		setName(name);
		setId(id);
	}

	public void setStock(int stockk) {
		this.stock = stockk;
	}

	public int getStock() {
		return stock;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

}
