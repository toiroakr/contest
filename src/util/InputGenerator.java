package util;

import java.util.Random;

/**
 * Created by higuchiakira on 2015/10/22.
 */
public class InputGenerator {
    public static void main(String[] args) {
        int n = randomInt(1, 199);
        n = n % 2 == 0 ? n + 1 : n;
        int c = randomInt(n, 1000);
        int f = randomInt(0, 200000);
        ln();

        int hoge = (int) Math.round((double) f / n) * 2;
        for (int i = 0; i < c; i++) {
            randomInt(1, hoge);
            randomInt(1, hoge);
            ln();
        }
    }

    public static int randomInt(int from, int to) {
        Random rand = new Random();
        int randI = rand.nextInt(to - from) + from;
        System.out.print(randI + " ");
        return randI;
    }

    public static void ln() {
        System.out.println();
    }

}
