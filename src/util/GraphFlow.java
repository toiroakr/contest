package util;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class GraphFlow {
	public static void main(String[] args) {
		FlowGraph g = new FlowGraph(4);
		g.addEdge(0, 1, 5);
		g.addEdge(0, 2, 3);
		g.addEdge(1, 3, 4);
		g.addEdge(2, 3, 5);
		System.out.println(g.maxFlow(0, 3));
	}
}

class FlowGraph {
	int maxV;
	List<Node> nodes = new ArrayList<Node>();
	int[] level; // 出発点からの距離
	int[] iter; // どこまで調べ終わったか

	public FlowGraph(int maxV) {
		this.maxV = maxV;
		level = new int[maxV];
		iter = new int[maxV];
		for (int i = 0; i < maxV; i++)
			nodes.add(new Node());
	}

	public void addEdge(int from, int to, int cap) {
		nodes.get(from).add(new Edge(to, cap, nodes.get(to).size()));
		nodes.get(to).add(new Edge(from, cap, nodes.get(from).size() - 1));
	}

	// sからの最小の移動回数の幅優先探索
	public void calcLevel(int s) {
		Deque<Node> que = new ArrayDeque<Node>();
		que.push(nodes.get(s));
		Node node;
		while ((node = que.poll()) != null) {
			for (Edge edge : node) {
				if (level[edge.to] == 0 && edge.capacity > 0) {
					level[edge.to] = level[nodes.indexOf(node)] + 1;
					que.push(nodes.get(edge.to));
				}
			}
		}
		level[s] = 0;
	}

	// 増加パスの深さ優先探索
	public int augmentingPath(int v, int t, int f) {
		if (v == t)
			return f;
		Node node = nodes.get(v);
		for (int i = iter[v]; i < node.size(); i++) {
			Edge edge = node.get(i);
			if (edge.capacity > 0 && level[v] < level[edge.to]) {
				int d = augmentingPath(edge.to, t, Math.min(f, edge.capacity));
				if (d > 0) {
					edge.capacity -= d;
					nodes.get(edge.to).get(edge.reverse).capacity += d;
					return d;
				}
			}
		}
		return 0;
	}

	public int maxFlow(int s, int t) {
		int flow = 0;
		//		for (;;) {
		calcLevel(s);
		if (level[t] == 0)
			return flow;
		int f;
		while ((f = augmentingPath(s, t, Integer.MAX_VALUE)) > 0)
			flow += f;
		return flow;
		//		}
	}

	// 節を表すクラス
	class Node extends ArrayList<Edge> {
	}

	// 辺を表すクラス(行き先，容量，逆潮流)
	class Edge {
		int to, capacity, reverse;

		public Edge(int to, int capacity, int reverse) {
			this.to = to;
			this.capacity = capacity;
			this.reverse = reverse;
		}
	}
}