package util;

import java.util.Arrays;

public class Mathematica {
	/**
	 * 階乗(mod)の配列を作成
	 */
	static long[] modFactArray(int size, long MOD) {
		long F[] = new long[size];
		F[0] = 1;
		for (int i = 1; i < F.length; i++) {
			F[i] = (F[i - 1] * (long) i) % MOD;
		}
		return F;
	}

	/**
	 * nCr(mod)を計算する
	 */
	static long modnCr(int n, int r, long F[], long MOD) {
		if (r > n - r) {

		}
		long X = F[n];
		long Y = (F[r] * F[n - r]) % MOD;

		return (X * (modPow(Y, MOD - 2, MOD) % MOD)) % MOD;
	}

	/**
	 * 累乗(mod)を計算する
	 */
	static long modPow(long X, long p, long MOD) {
		if (p == 0)
			return 1 % MOD;

		if (p % 2 == 0) {
			long v = modPow(X, p / 2, MOD);
			return (v * v) % MOD;
		} else {
			long v = modPow(X, p / 2, MOD);
			return (((v * v) % MOD) * X) % MOD;
		}
	}

	/**
	 * 最大公約数
	 */
	static int gcd(int a, int b) {
		return b == 0 ? a : gcd(b, a % b);
	}

	/**
	 * 最小公倍数 ※要gcd
	 */
	static int lcm(int a, int b) {
		return a * b / gcd(a, b);
	}

	/**
	 * 最長部分増加列の長さ ※要lowerBound
	 */
	static int lis(int[] ys) {
		final int n = ys.length;
		final int[] res = new int[n];
		Arrays.fill(res, Integer.MAX_VALUE);
		for (int i = 0; i < n; i++) {
			res[lowerBound(res, ys[i])] = ys[i];
		}
		return lowerBound(res, Integer.MAX_VALUE);
	}

	static int lowerBound(final int[] xs, final int x) {
		int low = 0, high = xs.length;
		while (low < high) {
			final int mid = (low + high) / 2;
			if (xs[mid] >= x)
				high = mid;
			else
				low = mid + 1;
		}
		return low;
	}

	/**
	 * ナップサック問題(制限重量内での最大の利益を計算)
	 * @param weight_prices weight_prices[i]でi番目の重さ，価格の組を取得
	 * @return 最大の利益
	 */
	static long napsack(int[][] weight_prices, int numOfItems, long weightLimit){
		long[][] dp = new long[numOfItems + 1][(int) (weightLimit + 1)];
		for (int i = numOfItems - 1; i >= 0; i--) {
			int[] weght_price = weight_prices[i];
			for (int j = 0; j <= weightLimit; j++) {
				if (j - weght_price[0] < 0)
					dp[i][j] = dp[i + 1][j];
				else
					dp[i][j] = Math.max(dp[i + 1][j], dp[i + 1][j - weght_price[0]]
							+ weght_price[1]);
			}
		}
		return dp[0][(int) weightLimit];
	}
	
	public static void dijkstra(int[][] map, int src, int[] distance) {
		int nTown = distance.length;
		boolean[] fixed = new boolean[nTown]; // 最短距離が確定したかどうか
		for (int i = 0; i < nTown; i++) { // 各都市について初期化する
			distance[i] = Integer.MAX_VALUE; // 最短距離の初期値は無限遠
			fixed[i] = false; // 最短距離はまだ確定していない
		}
		distance[src] = 0; // 出発地点までの距離を0とする
		while (true) {
			// 未確定の中で最も近い都市を求める
			int marked = minIndex(distance, fixed);
			if (marked < 0)
				return; // 全都市が確定した場合
			if (distance[marked] == Integer.MAX_VALUE)
				return; // 非連結グラフ
			fixed[marked] = true; // その都市までの最短距離は確定となる
			for (int j = 0; j < nTown; j++) { // 隣の都市(i)について
				if (map[marked][j] > 0 && !fixed[j]) { // 未確定ならば
					// 旗の都市を経由した距離を求める
					int newDistance = distance[marked] + map[marked][j];
					// 今までの距離よりも小さければ、それを覚える
					if (newDistance < distance[j])
						distance[j] = newDistance;
				}
			}
		}
	}

	// まだ距離が確定していない都市の中で、もっとも近いものを探す
	static int minIndex(int[] distance, boolean[] fixed) {
		int idx = 0;
		for (; idx < fixed.length; idx++)
			// 未確定の都市をどれか一つ探す
			if (!fixed[idx])
				break;
		if (idx == fixed.length)
			return -1; // 未確定の都市が存在しなければ-1
		for (int i = idx + 1; i < fixed.length; i++)
			// 距離が小さいものを探す
			if (!fixed[i] && distance[i] < distance[idx])
				idx = i;
		return idx;
	}
}
