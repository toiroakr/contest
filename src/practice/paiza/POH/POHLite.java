package practice.paiza.POH;

import practice.FastReader;

/*
 * POH Lite https://paiza.jp/poh/kirishima
 *
 * ■入力
 * m :（プロジェクトに必要な人員数）<\br>
 * n :（下請け会社の数）
 * q_1 r_1 :（1番目の下請け会社の人員数 発注に必要な費用[単位：万円]）
 * q_2 r_2 :（２番目の下請け会社の人員数 発注に必要な費用[単位：万円]）
 * ・・・
 * q_i r_i :（n 番目の下請け会社の人員数発注に必要な費用[単位：万円]）
 *
 * ■条件
 * 1 ≦ m ≦ 200000（プロジェクトに必要な人数：最大20万人）
 * 1 ≦ n ≦ 50（下請け会社数：最大50社）
 * 1 ≦ q_i ≦10000（各下請け会社の人員数：最大1万人）
 * 1 ≦ r_i ≦ 5000000（各下請け会社のへの発注費用[単位：万円]：１万円〜最大500億円）
 * m ≦ q_1 + q_2 + q_3 ... + q_i（各下請け会社の人員数の合計はプロジェクトに必要な人数 m 人以上になる）
 *
 * ■出力
 * 最もコストが安くなる組み合わせの総コストを出力して下さい。
 * 最後は改行し、余計な文字、空行を含んではいけません。
 *
 * ■例1
 * 60
 * 3
 * 40 4300
 * 30 2300
 * 20 2400
 * -> 6600
 *
 * ■例2
 * 250
 * 5
 * 35 3640
 * 33 2706
 * 98 9810
 * 57 5472
 * 95 7790
 * -> 23072
 *
 * @author Akira
 *
 */
public class POHLite {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static final String OK = "Possible";
	static final String NG = "Impossible";
	static FastReader fr = new FastReader();
	static final int INF = Integer.MAX_VALUE - 300000000;

	// どの会社に依頼しないかを考える
	static public void main(String args[]) throws Exception {
		int m = fr.nextInt();
		int n = fr.nextInt();
		int[][] qrs = fr.nextIntArray2D(n, 2);

		int sumOfWorkers = 0;
		int sumOfCost = 0;
		for (int[] qr : qrs) {
			sumOfWorkers += qr[0];
			sumOfCost += qr[1];
		}
		int margin = sumOfWorkers - m;

		System.out.println(sumOfCost - napsack(qrs, n, margin));;
	}

	static long napsack(int[][] weight_prices, int numOfItems, long weightLimit){
		long[][] dp = new long[numOfItems + 1][(int) (weightLimit + 1)];
		for (int i = numOfItems - 1; i >= 0; i--) {
			int[] weght_price = weight_prices[i];
			for (int j = 0; j <= weightLimit; j++) {
				if (j - weght_price[0] < 0)
					dp[i][j] = dp[i + 1][j];
				else
					dp[i][j] = Math.max(dp[i + 1][j], dp[i + 1][j - weght_price[0]]
							+ weght_price[1]);
			}
		}
		return dp[0][(int) weightLimit];
	}
}
