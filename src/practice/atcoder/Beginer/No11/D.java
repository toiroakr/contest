package practice.atcoder.Beginer.No11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class D {
	public static void main(String[] args) throws Exception, IOException {
		Reader sc = new Reader(System.in);
		int n = sc.nextInt();
		long d = sc.nextLong(), x = sc.nextLong(), y = sc.nextLong();

		if (x % d != 0 || y % d != 0) {
			System.out.println(0.0);
			return;
		}
		if (n - (x + y) / d < 0 || (n - (x + y) / d) % 2 == 1) {
			System.out.println(0.0);
			return;
		}

		long vain = (n - (x + y) / d) / 2;
		double p = 0;
		for (int i = 0; i <= vain; i++) {
			long down = i;
			long left = vain - down;
			long up = down + y / d;
			long right = left + x / d;
			double t = 1, N = n;
			for (int j = 0; j < up; j++, N--)
				t *= N / (4 * (up - j));
			for (int j = 0; j < down; j++, N--)
				t *= N / (4 * (down - j));
			for (int j = 0; j < left; j++, N--)
				t *= N / (4 * (left - j));
			for (int j = 0; j < right; j++, N--)
				t *= N / (4 * (right - j));
			// up,down,right,left,#ways
			db(up, down, right, left, t);
			p += t;
		}
		System.out.println(p);
	}

	static void db(Object... os) {
		System.err.println(Arrays.deepToString(os));

	}

	static long fact(long s, long n) {
		long i;
		if (n < 17) {
			long r = s;
			for (i = s + 1; i < s + n; i++)
				r *= i;
			return r;
		}
		i = n / 2;
		return fact(s, i) * fact(s + i, n - i);
	}

	static long factorial(long n) {
		return fact(1, n);
	}
}

class Reader {
	private BufferedReader x;
	private StringTokenizer st;

	public Reader(InputStream in) {
		x = new BufferedReader(new InputStreamReader(in));
		st = null;
	}

	public String nextString() throws IOException {
		while (st == null || !st.hasMoreTokens())
			st = new StringTokenizer(x.readLine());
		return st.nextToken();
	}

	public int nextInt() throws IOException {
		return Integer.parseInt(nextString());
	}

	public long nextLong() throws IOException {
		return Long.parseLong(nextString());
	}

	public double nextDouble() throws IOException {
		return Double.parseDouble(nextString());
	}
}