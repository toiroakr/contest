package practice.atcoder.Beginer.No11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class A {
	public static StringBuilder out = new StringBuilder();

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		int i = Integer.valueOf(in);
		br.close();

		System.out.println((i % 12) + 1);
	}
}