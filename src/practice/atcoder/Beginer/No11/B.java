package practice.atcoder.Beginer.No11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class B {
	public static StringBuilder out = new StringBuilder();

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		if (in.length() > 1)
			in = in.substring(0, 1).toUpperCase() + in.substring(1).toLowerCase();
		else
			in = in.toUpperCase();
		br.close();

		System.out.println(in);
	}
}