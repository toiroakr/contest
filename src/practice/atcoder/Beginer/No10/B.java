package practice.atcoder.Beginer.No10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class B {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		in = br.readLine();

		int sum = 0;
		String[] flowers = in.split(" ");
		for(String flower : flowers){
			int numOfPetal = Integer.valueOf(flower);
			switch (numOfPetal % 3) {
			case 0:
				if(numOfPetal % 2 == 0)
					sum+=3;
				break;
			case 1:
				if(numOfPetal % 2 == 0)
					sum++;
				break;
			case 2:
				sum++;
				if(numOfPetal % 2 == 1)
					sum++;
				break;
			default:
				break;
			}
		}
		System.out.println(sum);
		br.close();
	}
}
