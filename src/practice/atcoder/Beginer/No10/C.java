package practice.atcoder.Beginer.No10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class C {
	private static final int BEFORE_X = 0;
	private static final int BEFORE_Y = 1;
	private static final int AFTER_X = 2;
	private static final int AFTER_Y = 3;
	private static final int TIME = 4;
	private static final int VELOCITY = 5;
	private static final int WOMAN_X = 0;
	private static final int WOMAN_Y = 1;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		String[] tMoveStr = in.split(" ");
		int[] tMove = new int[6];
		for(int i = 0; i < 6; i++)
			tMove[i] = Integer.valueOf(tMoveStr[i]);
		double maxLen = tMove[TIME] * tMove[VELOCITY];
//		System.out.println(maxLen);
		in = br.readLine();
		int numOfWemen = Integer.valueOf(in);

		for(int i = 0; i < numOfWemen; i++){
//			System.out.println("woman #" + i);
			in = br.readLine();
			String[] womanXY = in.split(" ");
			int xDiff = Integer.valueOf(womanXY[WOMAN_X]) - tMove[BEFORE_X];
			int yDiff = Integer.valueOf(womanXY[WOMAN_Y]) - tMove[BEFORE_Y];
			double len = Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));
//			System.out.println(len);
			xDiff = Integer.valueOf(womanXY[WOMAN_X]) - tMove[AFTER_X];
			yDiff = Integer.valueOf(womanXY[WOMAN_Y]) - tMove[AFTER_Y];
			len += Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));
//			System.out.println(len);
			if(len <= maxLen){
				System.out.println("YES");
				System.exit(0);
			}
		}
		System.out.println("NO");
		br.close();
	}
}