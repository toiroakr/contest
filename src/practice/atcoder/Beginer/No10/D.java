package practice.atcoder.Beginer.No10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class D {
	public static List<Person> people;
	public static List<String> flow;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		int numOfPeople = Integer.parseInt(in.split(" ")[0]);
		people = new ArrayList<Person>();
		for (int i = 0; i < numOfPeople; i++) {
			people.add(new Person());
		}

		int numOfMarked = Integer.parseInt(in.split(" ")[1]);
		if (numOfMarked == 0) {
			System.out.println("0");
			System.exit(0);
		}
		int numOfRelation = Integer.parseInt(in.split(" ")[2]);

		in = br.readLine();
		List<Integer> marked = new ArrayList<Integer>();
		for (String markedPerson : in.split(" ")) {
			marked.add(Integer.valueOf(markedPerson));
		}

		for (int i = 0; i < numOfRelation; i++) {
			in = br.readLine();
			String[] friends = in.split(" ");
			int id_a = Integer.valueOf(friends[0]);
			int id_b = Integer.valueOf(friends[1]);
			people.get(id_a).addFriends(id_b);
			people.get(id_b).addFriends(id_a);
		}
		br.close();

		flow = new ArrayList<String>();
		Person takahashi = people.get(0);
		takahashi.search(new ArrayList<Integer>(), 0);
	}

}

class Person {
	List<Integer> friends = new ArrayList<Integer>();

	public void addFriends(int id) {
		friends.add(id);
	}

	public void search(ArrayList<Integer> path, int id) {
		path.add(id);
		ArrayList<Integer> copy = copy(path);
		boolean hasNext = false;
		for (Integer friend : friends) {
			if (!copy.contains(friend)) {
				D.people.get(friend).search(copy, friend);

				hasNext = true;
			}
		}
		if (!hasNext) {
			String pathStr = " ";
			for (Integer pid : copy)
				pathStr += pid + " ";
			D.flow.add(pathStr);
			System.out.println(pathStr);
		}
	}

	private ArrayList<Integer> copy(ArrayList<Integer> path) {
		ArrayList<Integer> copy = new ArrayList<Integer>();
		for(Integer i : path)
			copy.add(i);
		return copy;
	}
}
