package practice.atcoder.Beginer.No06;

import java.io.IOException;

import practice.FastReader;


public class C {
	static StringBuilder out = new StringBuilder();
	static final String LS = System.lineSeparator();
	static FastReader fr = new FastReader();
	static final String NOT_FOUND = "-1 -1 -1";

	public static void main(String[] args) throws Exception, IOException {
		int n = fr.nextInt();
		int m = fr.nextInt();

		if (n * 2 > m) {
			System.out.println(NOT_FOUND);
			System.exit(0);
		}
		if (n * 4 < m) {
			System.out.println(NOT_FOUND);
			System.exit(0);
		}

		int f2;
		int f3;
		int f4;
		double ave = m / (double) n;
		if (m == 3 * n) {
			System.out.println("0 " + n + " 0");
			System.exit(0);
		}
		if (ave > 3) {
			f2 = 0;
			f4 = m - 3 * n;
			f3 = n - f4;
		} else {
			f4 = 0;
			f3 = m - 2 * n;
			f2 = n - f3;
		}

		System.out.println(f2 + " " + f3 + " " + f4);
	}
}