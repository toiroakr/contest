package practice.atcoder.Beginer.No06;

import java.io.IOException;

import practice.FastReader;


public class B {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static FastReader fr = new FastReader();

	public static void main(String[] args) throws Exception, IOException {
		long n = fr.nextLong();

		long a1 = 0;
		long a2 = 0;
		long a3 = 1;

		if (n <= 1) {
			System.out.println("0");
			System.exit(0);
		}
		if (n == 3) {
			System.out.println("1");
			System.exit(0);
		}

		long an = 0;
		for (long i = 4; i <= n; i++) {
			an = a1 + a2 + a3;
			a1 = a2;
			a2 = a3;
			if (an > 10007) {
				an = an % 10007;
			}
			a3 = an;
		}

		System.out.println(an % 10007);
	}
}
