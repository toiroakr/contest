package practice.atcoder.Beginer.No06;

import java.io.IOException;

import practice.FastReader;


public class A {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static FastReader fr = new FastReader();

	public static void main(String[] args) throws Exception, IOException {
		String n = fr.next();
		if (n.indexOf("3") >= 0) {
			System.out.println("YES");
			System.exit(0);
		}

		int m = Integer.valueOf(n);
		if (m % 3 == 0)
			System.out.println("YES");
		else
			System.out.println("NO");

		// out.append(list.get(0) + ls);
		// System.out.println(out.toString());
		System.out.println();
	}
}
