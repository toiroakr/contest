package practice.atcoder.Beginer.No06;

import java.io.IOException;
import java.util.Arrays;

import practice.FastReader;


public class D {
	static FastReader fr = new FastReader();
	StringBuilder sb = new StringBuilder();
	static final String LS = System.lineSeparator();

	public static void main(String[] args) throws Exception, IOException {
		int n = fr.nextInt();

		int[] nums = fr.nextIntArray(n);
		System.out.println(n - LIS(nums));
	}

	private static int LIS(int[] ys) {
		final int n = ys.length;
		final int[] res = new int[n];
		Arrays.fill(res, Integer.MAX_VALUE);
		for (int i = 0; i < n; i++) {
			res[lowerBound(res, ys[i])] = ys[i];
		}
		return lowerBound(res, Integer.MAX_VALUE);
	}

	private static int lowerBound(final int[] xs, final int x) {
		int low = 0, high = xs.length;
		while (low < high) {
			final int mid = (low + high) / 2;
			final int cmp = xs[mid] < x ? -1 : xs[mid] == x ? 0 : 1;
			if (cmp >= 0)
				high = mid;
			else
				low = mid + 1;
		}
		return low;
	}

	static void db(Object... os) {
		System.err.println(Arrays.deepToString(os));
	}
}
