package practice.atcoder.KUPC.y2013;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

//KUPC 2013 F
public class F {
	public static StringBuilder out = new StringBuilder();

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		String[] split = in.split(" ");

		int n = Integer.valueOf(split[0]);
		//		int s = Integer.valueOf(split[1]);

		int end = 0;
		List<int[]> periods = new ArrayList<int[]>();
		for (int i = 0; i < n; i++) {
			in = br.readLine();
			split = in.split(" ");

			int l = Integer.valueOf(split[0]);
			int r = Integer.valueOf(split[1]);
			int[] period = { l, r };
			if (r > end)
				end = r;
			periods.add(period);
		}

		boolean allZero = true;
		List<int[]> distancesList = new ArrayList<int[]>();
		for (int i = 0; i < n; i++) {
			in = br.readLine();
			split = in.split(" ");

			int[] distances = new int[n];
			for (int j = 0; j < n; j++) {
				distances[j] = Integer.valueOf(split[j]);
				if (allZero && distances[j] != 0)
					allZero = true;
			}
			distancesList.add(distances);
		}

		if (allZero) {
			Collections.sort(periods, new Comparator<int[]>() {
				@Override
				public int compare(int[] o1, int[] o2) {
					if (o1[0] - o2[0] < 0)
						return -1;
					if (o1[0] - o2[0] > 0)
						return 1;
					if (o1[1] - o2[1] > 0)
						return -1;
					if (o1[1] - o2[1] > 0)
						return 1;
					return 0;
				}
			});
			int last = 0;
			int minus = 0;
			for (int[] period : periods) {
				int l_p = period[0];
				int r_p = period[1];
				if (l_p > last)
					minus = l_p - last;
				if (last < r_p)
					last = r_p;
			}
			System.out.println(last - minus);
		} else
			System.exit(0);

//		int planet = s - 1;
//		for (int i = 0; i < end; i++) {
//			int[] period = periods.get(planet);
//			int l_p = period[0];
//			int r_p = period[1];
//
//		}

		br.close();

		System.out.println();
	}
}
