package practice.atcoder.KUPC.y2014;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import practice.FastReader;


public class B {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static FastReader fr = new FastReader();

	static final String YES = "Y";
	static final String NO = "N";
	static final String Q = "? ";
	static final String A = "! ";

	public static void main(String[] args) throws Exception, IOException {
		List<Integer> list = new ArrayList<>();
		for (int i = 1; i <= 1000; i++)
			list.add(i);

		while (list.size() > 1) {
			int filter = list.get(1);
			System.out.println(Q + filter);
			boolean inverse = fr.next().equals(YES);
			filter(list,filter,inverse);
		}

		System.out.println(A + list.get(0));
	}

	static void filter(List<Integer> list, int divide, boolean inverse){
		List<Integer> removes = new ArrayList<>();
		for(Integer num : list){
			if(num % divide == 0 ^ inverse)
				removes.add(num);
		}
		list.removeAll(removes);
	}
}
