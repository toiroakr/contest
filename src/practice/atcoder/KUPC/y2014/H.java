package practice.atcoder.KUPC.y2014;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import practice.FastReader;


public class H {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	// static final String OK = "Possible";
	static final String NG = "-1";
	static final int INF = Integer.MAX_VALUE - 300000000;
	static FastReader fr = new FastReader();

	public static void main(String[] args) throws Exception, IOException {
		int n = fr.nextInt();
		@SuppressWarnings("unused")
		int l = fr.nextInt();
		int w = fr.nextInt();

		if (n <= 1)
			printExit("0");

		List<int[]> roads = fr.nextIntsList(n, 2);
		List<int[]> dp = new ArrayList<>();
		int[] dp_fl = new int[3];
		dp.add(dp_fl);
		for (int j = 1; j < roads.size(); j++) {
			// System.err.println(j);
			int[] road = roads.get(j);
			int[] dp_t = { INF, INF, j };

			for (int i = dp.size() - 1; i > -1; i--) {
				int[] dp_p = dp.get(i);
				int[] land = { dp_p[0] + w, roads.get(dp_p[2])[1] + w };
				if (road[1] < land[0])
					continue;
				if (land[1] < road[0] && land[1] - w != INF)
					break;
				dp_t[0] = Math.max(road[0], land[0]);
				dp_t[1] = Math.min(dp_p[1] + 1, dp_t[1]);
			}
			dp_fl = dp_t;
			if (dp_t[0] != INF)
				dp.add(dp_t);
		}
		if (dp_fl[1] >= n)
			System.out.println("-1");
		else
			System.out.println(dp_fl[1]);
	}

	static void printExit(String msg) {
		System.out.println(msg);
		System.exit(0);
	}
}
