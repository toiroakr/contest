package practice.atcoder.KUPC.y2014;

import java.io.IOException;
import java.util.List;

import practice.FastReader;


public class E {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static FastReader fr = new FastReader();
	static final String OK = "Possible";
	static final String NG = "Impossible";

	public static void main(String[] args) throws Exception, IOException {
		int n = fr.nextInt();

		List<int[]> w_hs = fr.nextIntsList(n, 2);
		for (int[] w_h : w_hs) {
			int w = w_h[0];
			int h = w_h[1];
			int matrix = w * h;
			if (matrix % 8 != 0 || matrix == 8)
				System.out.println(NG);
			else if (w < 2 || h < 2)
				System.out.println(NG);
			else
				System.out.println(OK);
		}
	}

	static void printExit(String msg) {
		System.out.println(msg);
		System.exit(0);
	}
}
