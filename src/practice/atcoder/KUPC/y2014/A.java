package practice.atcoder.KUPC.y2014;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import practice.FastReader;


public class A {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static FastReader fr = new FastReader();

	public static void main(String[] args) throws Exception, IOException {
		List<Integer> students = fr.nextIntList(3);
		List<Integer> chairs = fr.nextIntList(3);

		Collections.sort(students);
		Collections.sort(chairs);

		int sum = 0;
		for(int i = 0; i < 3; i++){
			int student = students.get(i);
			int chair = chairs.get(i);
			sum += Math.abs(chair - student);
		}
		System.out.println(sum);
	}
}
