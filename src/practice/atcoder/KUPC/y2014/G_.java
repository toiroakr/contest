package practice.atcoder.KUPC.y2014;

import java.io.IOException;

import practice.FastReader;


public class G_ {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static FastReader fr = new FastReader();

	public static void main(String[] args) throws Exception, IOException {
		int n = fr.nextInt();
		int d = fr.nextInt();

		for (int i = 0; i < n; i++) {
			int pattern = (i / d) % 3;
			for (int j = i; i < j + d && i < n; i++) {
				switch (pattern) {
				case 0:
					out.append(0);
					break;
				case 1:
					out.append((i % 2));
					break;
				case 2:
					out.append(1);
					break;
				}
			}
		}
		System.out.println(out.toString());

		String kyoko = fr.next();
		String yui = fr.next();

		System.out.println("Move(A,1)");
		kyoko += fr.next();
		System.out.println("Move(A,1)");
		kyoko += fr.next();

		System.out.println("Move(B,1)");
		yui += fr.next();
		System.out.println("Move(B,1)");
		yui += fr.next();

		int k = evel(kyoko);
		int y = evel(yui);
		if(k > y)
			System.out.println("i>j");
		else
			System.out.println("i<j");
	}

	private static int evel(String str) {
		switch (str) {
		case "000":
			return 0;
		case "001":
			return 1;
		case "010":
			return 2;
		case "011":
			return 3;
		case "100":
			return 4;
		case "101":
			return 5;
		case "110":
			return 6;
		case "111":
			return 7;
		}
		return -1;
	}

	static void printExit(String msg) {
		System.out.println(msg);
		System.exit(0);
	}
}