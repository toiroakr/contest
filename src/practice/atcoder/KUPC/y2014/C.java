package practice.atcoder.KUPC.y2014;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import practice.FastReader;


public class C {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static FastReader fr = new FastReader();
	static Num NAN = new Num(-1);
	static Num[] kyoko;
	static Num[] yui;

	public static void main(String[] args) throws Exception, IOException {
		int n = fr.nextInt();
		int m = fr.nextInt();
		int q = fr.nextInt();

		kyoko = new Num[n];
		yui = new Num[m];
		List<int[]> promises = fr.nextIntsList(q, 2);
		Collections.sort(promises, new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				if (o1[0] != o2[0])
					return o1[0] - o2[0];
				if (o1[1] != o2[1])
					return o1[1] - o2[1];
				else
					return 0;
			}
		});

		int num = 0;
		for (int[] promise : promises) {
			if (kyoko[promise[0] - 1] != null && yui[promise[1] - 1] != null) {
				if (yui[promise[1] - 1].i < kyoko[promise[0] - 1].i) {
					set(kyoko[promise[0] - 1], yui[promise[1] - 1]);
				}
				if (yui[promise[1] - 1].i > kyoko[promise[0] - 1].i) {
					set(yui[promise[1] - 1], kyoko[promise[0] - 1]);
				}
			} else if (kyoko[promise[0] - 1] != null) {
				yui[promise[1] - 1] = kyoko[promise[0] - 1];
			} else if (yui[promise[1] - 1] != null)
				kyoko[promise[0] - 1] = yui[promise[1] - 1];
			else {
				kyoko[promise[0] - 1] = new Num(num);
				yui[promise[1] - 1] = kyoko[promise[0] - 1];
				num++;
			}
		}

		if (end(kyoko, yui))
			printExit("" + 0);
		int i = 0;
		int latest_renew = -1;
		int max = n * m + 1;
		while (i < max) {
			if (kyoko[i % n] == null && yui[i % m] == null) {
				kyoko[i % n] = new Num(num);
				yui[i % m] = kyoko[i % n];
				num++;
				latest_renew = i;
			} else if (kyoko[i % n] == null && yui[i % m] != null) {
				kyoko[i % n] = yui[i % m];
				latest_renew = i;
			} else if (kyoko[i % n] != null && yui[i % m] == null) {
				yui[i % m] = kyoko[i % n];
				latest_renew = i;
			} else {
				if (kyoko[i % n].i < yui[i % m].i) {
					set(yui[i % m], kyoko[i % n]);
					latest_renew = i;
				} else if (kyoko[i % n].i > yui[i % m].i) {
					set(kyoko[i % n], yui[i % m]);
					latest_renew = i;
				} else if (end(kyoko, yui))
					printExit("" + i);
				if (end(kyoko, yui))
					printExit("" + ++i);
			}
			i++;
		}
		System.out.println(latest_renew + 1);
	}

	static void set(final Num a, final Num b) {
		for (int i = 0; i < kyoko.length; i++)
			if (kyoko[i] != null && kyoko[i].equals(a))
				kyoko[i] = b;
		for (int i = 0; i < yui.length; i++)
			if (yui[i] != null && yui[i].equals(a))
				yui[i] = b;
	}

	private static boolean end(Num[] kyoko, Num[] yui) {
		Num num = kyoko[0];
		for (Num i : kyoko)
			if (i == null)
				return false;
			else if (!i.equals(num))
				return false;
		for (Num i : yui)
			if (i == null)
				return false;
			else if (!i.equals(num))
				return false;
		return true;
	}

	static void printExit(String msg) {
		System.out.println(msg);
		System.exit(0);
	}
}

class Num {
	int i;

	Num(int i) {
		this.i = i;
	}

	boolean equals(Num n) {
		return i == n.i;
	}

	void set(Num n) {
		i = n.i;
	}

	boolean hasNum() {
		return i != -1;
	}

	void increment() {
		i++;
	}
}
