package practice.atcoder.KUPC.y2014;

import java.io.IOException;

import practice.FastReader;


public class D {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static final String OK = "Possible";
	static final String NG = "Impossible";
	static FastReader fr = new FastReader();
	static final int INF = Integer.MAX_VALUE - 300000000;
	static final int MOD = (int) (1e9 + 7);

	// KUPC2014 D
	public static void main(String[] args) throws Exception, IOException {
		// System.out.println(comb(19,5));
		// System.exit(0);

		// バイナリ1と目標とのハミング距離
		String b1 = fr.next();
		int d1 = fr.nextInt();
		// バイナリ2と目標とのハミング距離
		String b2 = fr.next();
		int d2 = fr.nextInt();
		int l = b1.length();

		// 2つのバイナリのハミング距離
		int d = 0;
		for (int i = 0; i < l; i++) {
			if (b1.charAt(i) != b2.charAt(i))
				d++;
		}
		// 2つのバイナリの共通部分長
		int s = l - d;

		// 例外処理(無駄がありそう)
		int d_d = Math.abs(d1 - d2);
		if (d_d > d)
			printExit("0");
		if ((d - d_d) % 2 != 0)
			printExit("0");
		if (d1 + d2 < d)
			printExit("0");
		if ((d1 + d2 - d) % 2 != 0)
			printExit("0");
		if ((d1 + d2 - d) % 2 > s)
			printExit("0");

		long F[] = modFactArray(l, MOD);

		// 共通部分の選び方
		long p_s = 1;
		p_s = modnCr(s, (d1 + d2 - d) / 2, F, MOD);

		// 相違部分の選び方
		long p_d = 1;
		p_d = modnCr(d, (d_d + d) / 2, F, MOD);

		long res = (p_d * p_s) % MOD;
		System.out.println(res);
	}

	/**
	 * 階乗(mod)の配列を作成
	 */
	static long[] modFactArray(int size, long MOD) {
		long F[] = new long[size];
		F[0] = 1;
		for (int i = 1; i < F.length; i++) {
			F[i] = (F[i - 1] * (long) i) % MOD;
		}
		return F;
	}

	/**
	 * nCr(mod)を計算する
	 */
	static long modnCr(int n, int r, long F[], long MOD) {
		if (r > n - r) {
			printExit("0");
		}
		long X = F[n];
		long Y = (F[r] * F[n - r]) % MOD;

		return (X * (modPow(Y, MOD - 2, MOD) % MOD)) % MOD;
	}

	/**
	 * 累乗(mod)を計算する
	 */
	static long modPow(long X, long p, long MOD) {
		if (p == 0)
			return 1 % MOD;

		if (p % 2 == 0) {
			long v = modPow(X, p / 2, MOD);
			return (v * v) % MOD;
		} else {
			long v = modPow(X, p / 2, MOD);
			return (((v * v) % MOD) * X) % MOD;
		}
	}

	static void printExit(String msg) {
		System.out.println(msg);
		System.exit(0);
	}
}
