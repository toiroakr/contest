package practice.atcoder.KUPC.y2012;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class A {
	public static StringBuilder out = new StringBuilder();

	static int n;
	static int m;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		String[] sp = in.split(" ");
		n = Integer.valueOf(sp[0]);
		m = Integer.valueOf(sp[1]);

		int[][] prof = new int[m][2];
		for (int i = 0; i < m; i++) {
			in = br.readLine();
			sp = in.split(" ");
			prof[i][0] = Integer.valueOf(sp[0]) - 1;
			prof[i][1] = Integer.valueOf(sp[1]) - 1;
		}

		int c = 0;
		for(int i = 0; i < n; i++){
			i = search(i, prof);
			if(i < 0){
				System.out.println("Impossible");
				return;
			}
			c++;
		}

		br.close();

		System.out.println(c);
	}

	private static int search(int i, int[][] prof) {
		int max = Integer.MIN_VALUE;
		for(int j = 0; j < m; j ++){
			int s = prof[j][0];
			int t = prof[j][1];
			if(s > i)
				continue;
			if(max < t){
				max = t;
			}
		}
		if(max < i)
			return -1;
		return max;
	}
}
