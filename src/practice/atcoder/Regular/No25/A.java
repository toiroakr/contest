package practice.atcoder.Regular.No25;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class A {

	public static StringBuilder out = new StringBuilder();

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		String[] sp1 = in.split(" ");
		int[] mine1 = new int[7];
		for (int i = 0; i < mine1.length; i++)
			mine1[i] = Integer.valueOf(sp1[i]);

		String in2 = br.readLine();
		String[] sp2 = in2.split(" ");
		int[] mine2 = new int[7];
		for (int i = 0; i < mine2.length; i++)
			mine2[i] = Integer.valueOf(sp2[i]);

		int sum = 0;
		for (int i = 0; i < mine1.length; i++) {
			if (mine1[i] > mine2[i]) {
				sum += mine1[i];
			} else {
				sum += mine2[i];
			}
		}

		br.close();

		System.out.println(sum);
		//		out.append(in + "\n");
		//		System.out.println(out.toString());
	}
}
