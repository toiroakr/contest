package practice.atcoder.Regular.No25;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;

public class C {
	public static StringBuilder out = new StringBuilder();

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String in = br.readLine();
		String[] sp = in.split(" ");
		int n = Integer.valueOf(sp[0]);
		// int m = Integer.valueOf(sp[1]);
		// int r = Integer.valueOf(sp[2]);
		// int t = Integer.valueOf(sp[3]);

		Graph graph = new Graph(n);
		while ((in = br.readLine()) != null) {
			sp = in.split(" ");
			int from = Integer.valueOf(sp[0]);
			int to = Integer.valueOf(sp[1]);
			int length = Integer.valueOf(sp[2]);
			graph.addEdge(from, to, length);
		}
		graph.minMove();

		br.close();

		out.append(in + "\n");
		System.out.println(out.toString());
	}
}

class Graph {
	int nodeNum;
	List<Node> nodes = new ArrayList<Node>();

	public Graph(int nodeNum) {
		this.nodeNum = nodeNum;
		for (int i = 0; i < nodeNum; i++)
			nodes.add(new Node());
	}

	public HashMap<String, Integer> minMove() {
		HashMap<String, Integer> map = new HashMap<>();
		for (int i = 0; i < nodeNum - 1; i++) {
//			int minPath = dijkstra(i);
			for (int j = i + 1; j < nodeNum; j++) {
//				String path = i + "-" + j;
			}
		}
		return map;
	}

	@SuppressWarnings("unused")
	private int dijkstra(int from) {
		Node n = nodes.get(from);
		int[] fixed = new int[nodeNum];
		Deque<Node> que = new ArrayDeque<Node>();
		que.push(n);
		Node node;
		int[] length = new int[nodeNum];
		int min = 0;
		while ((node = que.poll()) != null) {
			for (Edge edge : node) {
				if (length[edge.to] == 0) {
					length[edge.to] = length[nodes.indexOf(node)] + 1;
					que.push(nodes.get(edge.to));
				}
			}
		}
		return min;
	}

	public void addEdge(int from, int to, int length) {
		nodes.get(from).add(new Edge(to, length));
		nodes.get(to).add(new Edge(from, length));
	}

	// �?��表すクラス
	class Node extends ArrayList<Edge> {
	}

	// 辺を表すクラス(行き先，距離)
	class Edge {
		int to, capacity;

		public Edge(int to, int length) {
			this.to = to;
			this.capacity = length;
		}
	}
}
