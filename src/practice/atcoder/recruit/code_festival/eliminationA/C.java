package practice.atcoder.recruit.code_festival.eliminationA;

import java.io.IOException;
import practice.FastReader;

public class C {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static final String OK = "Possible";
	static final String NG = "Impossible";
	static FastReader fr = new FastReader();
	static final int INF = Integer.MAX_VALUE - 300000000;

	public static void main(String[] args) throws Exception, IOException {
		long a = fr.nextLong() - 1;
		long b = fr.nextLong();

		long d4 = a / 4;
		long d100 = a / 100;
		long d400 = a / 400;
		long am = d4 - d100 + d400;

		d4 = b / 4;
		d100 = b / 100;
		d400 = b / 400;
		long bm = d4 - d100 + d400;

		System.out.println(bm - am);
	}

	static void printExit(String msg) {
		System.out.println(msg);
		System.exit(0);
	}
}