package practice.atcoder.recruit.code_festival.eliminationA;

import java.io.IOException;

import practice.FastReader;

public class B {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static final String OK = "Possible";
	static final String NG = "Impossible";
	static FastReader fr = new FastReader();
	static final int INF = Integer.MAX_VALUE - 300000000;

	public static void main(String[] args) throws Exception, IOException {
		String s = fr.next();
		int a = s.length();
		long b = fr.nextLong();

		long d = (b - 1) % a;

		System.out.println(s.charAt((int) d));
	}

	static void printExit(String msg) {
		System.out.println(msg);
		System.exit(0);
	}
}