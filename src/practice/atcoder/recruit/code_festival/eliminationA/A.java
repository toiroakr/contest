package practice.atcoder.recruit.code_festival.eliminationA;

import java.io.IOException;

import practice.FastReader;

public class A {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static final String OK = "Possible";
	static final String NG = "Impossible";
	static FastReader fr = new FastReader();
	static final int INF = Integer.MAX_VALUE - 300000000;

	public static void main(String[] args) throws Exception, IOException {
		String n = fr.next();

		System.out.println(n + "2014");
	}

	static void printExit(String msg) {
		System.out.println(msg);
		System.exit(0);
	}
}