package practice.atcoder.recruit.code_festival.relay;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import practice.FastReader;

public class F {
	static FastReader fr = new FastReader();

	public static void main(String[] args) throws Exception, IOException {
		int n = fr.nextInt();
		Map<Integer, Node> graph = new HashMap<>();
		Map<Integer, Boolean> singleEdges = new HashMap<>();
		for (int i = 0; i < n; i++) {
			graph.put(i, new Node(i));
			singleEdges.put(i, true);
		}

		int[][] links = fr.nextIntArray2D(n, 2);
		for (int[] link : links) {
			int id1 = link[0] - 1;
			int id2 = link[1] - 1;
			Node n1 = graph.get(id1);
			Node n2 = graph.get(id2);

			n1.addLink(n2);
			n2.addLink(n1);

			singleCheck(id1, singleEdges);
			singleCheck(id2, singleEdges);
		}

		for (Integer singleEdge : singleEdges.keySet()) {
			boolean noLink = singleEdges.get(singleEdge);
			if (noLink) {
				graph.remove(singleEdge);
				continue;
			}
			Node node = graph.get(singleEdge);
			node.remove(graph);
		}

		System.out.println(graph.size());
	}

	private static void singleCheck(int id1, Map<Integer, Boolean> singleEdge) {
		Boolean single = singleEdge.get(id1);
		if (single != null) {
			if (single)
				singleEdge.put(id1, false);
			else
				singleEdge.remove(id1);
		}
	}
}

class Node {
	int id;
	Map<Integer, Node> links = new HashMap<>();

	Node(int id) {
		this.id = id;
	}

	public void remove(int id) {
		links.remove(id);
	}

	public void remove(Map<Integer, Node> graph) {
		if (links.size() == 1) {
			for (Node linkNode : links.values()) {
				linkNode.remove(id);
				linkNode.remove(graph);
			}
			graph.remove(id);
		}
	}

	void addLink(Node to) {
		if (!links.containsKey(to.id))
			links.put(to.id, to);
	}
}
