package practice.atcoder.recruit.code_festival.eliminationB;

import java.io.IOException;

import practice.FastReader;

public class C {
	static StringBuilder out = new StringBuilder();
	static String ls = System.lineSeparator();
	static final String OK = "YES";
	static final String NG = "NO";
	static FastReader fr = new FastReader();
	static final int INF = Integer.MAX_VALUE - 300000000;

	public static void main(String[] args) throws Exception, IOException {
		char[] s1 = fr.nextCharArray();
		char[] s2 = fr.nextCharArray();
		char[] s3 = fr.nextCharArray();
		int n = s1.length / 2;

		int[][] ss = new int['Z' - 'A' + 1][3];
		for (int i = 0; i < 2 * n; i++) {
			ss[s1[i] - 'A'][0]++;
			ss[s2[i] - 'A'][1]++;
			ss[s3[i] - 'A'][2]++;
		}

		int min = 0, max = 0;
		for (int i = 0; i < ss.length; i++) {
			int c1 = ss[i][0];
			int c2 = ss[i][1];
			int c3 = ss[i][2];
			if (c1 + c2 < c3)
				printExit(NG);
			min += Math.max(0, c3 - c2);
			max += Math.min(c1, c3);
		}

		if (min <= n && n <= max)
			printExit(OK);
		System.out.println(NG);
	}

	static void printExit(String msg) {
		System.out.println(msg);
		System.exit(0);
	}
}
