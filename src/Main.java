import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.List;
import java.util.PriorityQueue;

public class Main {
    static FastReader in = new FastReader();

    public static void main(String[] args) throws Exception, IOException {
        Main main = new Main();
        main.run();
    }

    void run() {
        int n = in.nextInt();
        int c = in.nextInt();
        int f = in.nextInt();

        //全体:CSAT低い順
        Calf[] calves = new Calf[c];
        for (int i = 0; i < c; i++) {
            calves[i] = new Calf();
            calves[i].csat = in.nextInt();
            calves[i].aid = in.nextInt();
        }
        Arrays.sort(calves, new Comparator<Calf>() {
            public int compare(Calf o1, Calf o2) {
                return o1.csat - o2.csat;
            }
        });
//        System.out.println(Arrays.toString(calves));


        PriorityQueue<Calf> current = new PriorityQueue<Calf>(n, new Comparator<Calf>() {
            @Override
            public int compare(Calf o1, Calf o2) {
                return o2.aid - o1.aid;
            }
        });

        int[] l = new int[c];
        //CSAT順にn/2匹とる
        int sum = 0;
        int i = 0;
        n /= 2;
        for (; i < n; i++) {
            Calf calf = calves[i];
            current.add(calf);
            sum += calf.aid;
        }
        l[i - 1] = sum;

        for (; i < c - n; i++) {
            Calf calf = calves[i];
            Calf high = current.peek();
            if (calf.aid < high.aid) {
                current.remove(high);
                current.add(calf);
                sum += calf.aid - high.aid;
            }
            l[i] = sum;
        }

        int[] h = new int[c];
        sum = 0;
        current.clear();
        for (i = c - 1; i >= c - n; i--) {
            Calf calf = calves[i];
            current.add(calf);
            sum += calf.aid;
        }
        h[i + 1] = sum;

        for (; i >= n; i--) {
            Calf calf = calves[i];
            Calf high = current.peek();
            if (calf.aid < high.aid) {
                current.remove(high);
                current.add(calf);
                sum += calf.aid - high.aid;
            }
            h[i] = sum;
        }

        int ans = -1;
//        System.out.println(Arrays.toString(l));
//        System.out.println(Arrays.toString(h));
        for (i = c - n - 1; i >= n; i--) {
            Calf calf = calves[i];
//            System.out.println(i);
//            System.out.println("l:" + l[i - 1] + " p:" + calf.aid + " h:" + h[i + 1]);
            if (l[i - 1] + calf.aid + h[i + 1] <= f) {
                printExit(calf.csat);
            }
        }
        System.out.println(ans);
    }

    void printExit(Object msg) {
        System.out.println(String.valueOf(msg));
        System.exit(0);
    }
}

class Calf {
    public int csat;
    public int aid;

    public String toString() {
        return "c:" + csat + "/a:" + aid;
    }
}

class FastReader {
    private InputStream in = System.in;
    private byte[] buf = new byte[1024];
    private int charNum;
    private int charLen;
    private StringBuilder sb = new StringBuilder();

    public int read() {
        if (charLen == -1)
            throw new InputMismatchException();
        if (charNum >= charLen) {
            charNum = 0;
            try {
                charLen = in.read(buf);
            } catch (IOException e) {
                throw new InputMismatchException();
            }
            if (charLen <= 0)
                return -1;
        }
        return buf[charNum++];
    }

    public String next() {
        int c = read();
        while (isWhitespace(c)) {
            c = read();
        }
        sb.setLength(0);
        do {
            sb.appendCodePoint(c);
            c = read();
        } while (!isWhitespace(c));
        return sb.toString();
    }

    public char[] nextCharArray() {
        return next().toCharArray();
    }

    public int nextInt() {
        return (int) nextLong();
    }

    public int[] nextIntArray(int n) {
        int[] array = new int[n];
        for (int i = 0; i < n; i++)
            array[i] = nextInt();
        return array;
    }

    public List<Integer> nextIntList(int n) {
        Integer[] array = new Integer[n];
        for (int i = 0; i < n; i++)
            array[i] = nextInt();
        return Arrays.asList(array);
    }

    public int[][] nextIntArray2D(int n, int m) {
        int[][] array = new int[n][m];
        for (int i = 0; i < n; i++)
            array[i] = nextIntArray(m);
        return array;
    }

    public List<int[]> nextIntsList(int n, int m) {
        List<int[]> list = new ArrayList<int[]>(n);
        for (int i = 0; i < n; i++)
            list.add(nextIntArray(m));
        return list;
    }

    public long nextLong() {
        int c = read();
        while (isWhitespace(c)) {
            c = read();
        }
        int sgn = 1;
        if (c == '-') {
            sgn = -1;
            c = read();
        }
        long res = 0;
        do {
            if (c < '0' || c > '9')
                throw new InputMismatchException();
            res *= 10;
            res += c - '0';
            c = read();
        } while (!isWhitespace(c));
        return res * sgn;
    }

    public double nextDouble() {
        return Double.parseDouble(next());
    }

    public double[] nextDoubleArray(int n) {
        double[] array = new double[n];
        for (int i = 0; i < n; i++)
            array[i] = nextDouble();
        return array;
    }

    public boolean isWhitespace(int c) {
        return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
    }
}
