# 問題リスト
##AtCoder
###Beginer Content
- **[ABC #010](http://abc010.contest.atcoder.jp/assignments)**－[解説](http://www.slideshare.net/chokudai/abc010-35598499)

###Regular Content
- **[ARC #025](http://arc025.contest.atcoder.jp/assignments)**

###Kyoto University Programming Contest

 - **[KUPC 2012(A,D)](http://kupc2012.contest.atcoder.jp/assignments)**－[解説](http://www.kupc.jp/2012.html)
 - **[KUPC 2013(F)](http://kupc2013.contest.atcoder.jp/assignments)**－[解説](http://www.kupc.jp/2013.html)
